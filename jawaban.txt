1.Membuat Database
     create database myshop;
2.Membuat Table di Dalam Database
    -users
      create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar (255),
    -> password varchar (255)
    -> );
Query OK, 0 rows affected (0.013 sec)
    MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(8)       | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.005 sec)

-categories
    MariaDB [myshop]> create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar (255)
    -> );
Query OK, 0 rows affected (0.009 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(8)       | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.004 sec)

    -items
        MariaDB [myshop]> create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar (255),
    ->  description varchar(255),
    -> price int(20),
    ->  stock int(50),
    -> category_id int(20),
    -> foreign key(category_id) references categories (id)
    -> );
Query OK, 0 rows affected (0.011 sec)

MariaDB [myshop]> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(8)       | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(20)      | YES  |     | NULL    |                |
| stock       | int(50)      | YES  |     | NULL    |                |
| category_id | int(20)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.005 sec)

3.Memasukkan Data pada Table
    MariaDB [myshop]> insert into users(name,email,password)
    -> values
    -> ("John Doe","john@doe.com","john123"),
    -> ("jane Doe","jane@doe.com","jenita123");
Query OK, 2 rows affected (0.003 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

MariaDB [myshop]> insert into categories(name)
    -> values
    -> ("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.001 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.000 sec)

MariaDB [myshop]> insert into items(name,description,price,stock,category_id)
    -> values
    -> ("Sumsung b50","hp keren dari merek samsung",4000000,100,1),
    -> ("Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);
Query OK, 3 rows affected (0.003 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select*from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsung b50 | hp keren dari merek samsung       | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)

MariaDB [myshop]>
4. Mengambil Data dari Database
    a. Mengambil data users
    MariaDB [myshop]> select id,name,email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.000 sec)
    b.Mengambil data items
    MariaDB [myshop]> select * from items where price>1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsung b50 | hp keren dari merek samsung       | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.002 sec)

- menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join).
    MariaDB [myshop]> select items.name,items.description,items.price,items.stock,
    items.category_id,categories.name as kategori from items inner join categories on items.category_id = categories.id;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| Sumsung b50 | hp keren dari merek samsung       | 4000000 |   100 |           1 | gadget   |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.002 sec)
- Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. 
  Masukkan query pada text jawaban di nomor ke 5.
MariaDB [myshop]> update items set price=2500000 where id = 1;
Query OK, 1 row affected (0.002 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsung b50 | hp keren dari merek samsung       | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.000 sec)